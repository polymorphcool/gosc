/* register_types.cpp */

#include "register_types.h"

#include "OSCmessage.h"
#include "OSCreceiver.h"
#include "OSCsender.h"

void register_gosc_types() {
  ClassDB::register_class<OSCreceiver>();
  ClassDB::register_class<OSCmessage>();
  ClassDB::register_class<OSCsender>();
}

void unregister_gosc_types() {
  // nothing to do here
}
